let _stage   = []
let _enemies = []

let _rock;
let _roll;


function init() {
  _rock = new Rock()
  _roll = new Roll()

  _stage.push(_rock)
  _stage.push(_roll)

  for (let i = 0; i < 15; i++) {
    let e = new Enemy(Math.random() * WIDTH, Math.random() * HEIGHT)
    _stage.push(e)
    _enemies.push(e)
  }
}

function update() {
  _stage.forEach(c => c.update())

  for (let i = _enemies.length - 1; i >= 0; i--) {
    let e = _enemies[i]

    if (testPointOnLine(e, [_rock, _roll])) {
      _enemies.splice(i, 1)
      _stage.splice(_stage.indexOf(e), 1)
    }
  }
}

function draw() {
  cls()

  line(_rock.x, _rock.y, _roll.x, _roll.y, 7)

  _stage.forEach(c => c.draw())
}

function testPointOnLine(p, line) {
  if (!testPointInBoundingBox(p, line)) {
    return false
  }

  let slope = (line[1].y - line[0].y) / (line[1].x - line[0].x)
  let b     = line[0].y - (slope * line[0].x)

  let lineX = (p.y - b) / slope
  let lineY = (slope * p.x) + b

  let diffX = Math.abs(p.x - lineX)
  let diffY = Math.abs(p.y - lineY)

  return diffX < 1 || diffY < 1 
}

function testPointInBoundingBox(p, polygon) {
  let minX = polygon[0].x
  let maxX = polygon[0].x
  let minY = polygon[0].y
  let maxY = polygon[0].y

  for (let i = 1; i < polygon.length; i++) {
    let q = polygon[i]
    minX = Math.min(q.x, minX)
    maxX = Math.max(q.x, maxX)
    minY = Math.min(q.y, minY)
    maxY = Math.max(q.y, maxY)
  }

  if (p.x < minX || p.x > maxX || p.y < minY || p.y > maxY) {
    return false
  }
  return true
}

class Sprite {
  constructor(x, y) {
    this.x = x
    this.y = y
  }
  update() {}
  draw() {}
}

class Rock extends Sprite {
  constructor() {
    super(45, 64)
    this.SPEED = 0.25
  }
  
  update() {
    if (btn('w')) {
      this.y -= this.SPEED
    }
    if (btn('a')) {
      this.x -= this.SPEED
    }
    if (btn('s')) {
      this.y += this.SPEED
    }
    if (btn('d')) {
      this.x += this.SPEED
    }
  }

  draw() {
    circfill(this.x, this.y, 3, 5)
  }
}

class Roll extends Sprite {
  constructor() {
    super(64, 64)
    this.SPEED = 1
  }

  update() {
    if (btn('arrowup')) {
      this.y -= this.SPEED
    }
    if (btn('arrowleft')) {
      this.x -= this.SPEED
    }
    if (btn('arrowdown')) {
      this.y += this.SPEED
    }
    if (btn('arrowright')) {
      this.x += this.SPEED
    }
  }

  draw() {
    circfill(this.x, this.y, 2, 8)
  }
}

class Enemy extends Sprite {
  constructor(x, y) {
    super(x, y)
  }

  draw() {
    pset(this.x, this.y, 14)
  }
}
