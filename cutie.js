(function(ns) {

  const COLORS_HEX = [
    '000000', // 0
    '1D2B53', // 1
    '7E2553', // 2
    '008751', // 3
    'AB5236', // 4
    '5F574F', // 5
    'C2C3C7', // 6 
    'FFF1E8', // 7
    'FF004D', // 8
    'FFA300', // 9 
    'FFEC27', // 10
    '00E436', // 11
    '29ADFF', // 12
    '83769C', // 13
    'FF77A8', // 14
    'FFCCAA'  // 15
  ];

  const COLORS        = [];
  const FPS_SMOOTHING = 0.9;
  const KEY_STATE     = {};

  ns.WIDTH  = 128;
  ns.HEIGHT = 128;

  ns.time   = 0;
  ns.fps    = 0;
  ns.mouseX = 0;
  ns.mouseY = 0;

  let _canvas;
  let _ctx;
  let _lastFrameTime;
  let _avgFrameTime;
  let _imgData;
  let _buffer;

  ns._init = function() {
    _canvas        = document.getElementById('canvas');
    _ctx           = _canvas.getContext('2d');
    _lastFrameTime = new Date().getTime();
    _avgFrameTime  = 16;
    _imgData       = _ctx.createImageData(WIDTH, HEIGHT);
    _buffer        = _imgData.data;

    _ctx.imageSmoothingEnabled = false;
    _ctx.font = '10px pixel';

    _initColors();
    _initInput();

    setInterval(_onFrame, 16);

    ns.init();

    ns.cls();
  }

  function _initColors() {
    for (let i = 0; i < COLORS_HEX.length; i++) {
      let value = parseInt(COLORS_HEX[i], 16);
      COLORS.push([
        (value >> 16) & 255,
        (value >>  8) & 255,
        (value >>  0) & 255
      ]);
    }
  }

  function _initInput() {
    _canvas.onmousemove= function(e) {
      let widthRatio  = WIDTH  / _canvas.offsetWidth;
      let heightRatio = HEIGHT / _canvas.offsetHeight;

      ns.mouseX = Math.floor((e.clientX - _canvas.offsetLeft) * widthRatio);
      ns.mouseY = Math.floor((e.clientY - _canvas.offsetTop) * heightRatio);
    }
      
    window.onkeydown = function(e) {
      KEY_STATE[e.key.toLowerCase()] = true;
    }

    window.onkeyup = function(e) {
      KEY_STATE[e.key.toLowerCase()] = false;
    }
  }

  function _onFrame() {
    _calculateFps();
    _update();
    _draw();
  }

  function _calculateFps() {
    let currentTime        = new Date().getTime();
    let timeSinceLastFrame = currentTime - _lastFrameTime;

    _avgFrameTime  = (_avgFrameTime * FPS_SMOOTHING) + (timeSinceLastFrame * (1 - FPS_SMOOTHING));
    _lastFrameTime = currentTime;

    ns.fps = Math.min(1000 / _avgFrameTime, 60);
  }

  function _update() {
    ns.time++;
    ns.update();
  }

  function _draw() {
    ns.draw();
    _ctx.putImageData(_imgData, 0, 0);
  }

  // =====================
  // =======  API  ======= 
  // =====================

  ns.log = function(msg) {
    console.log(msg);
  }

  ns.btn = function(b) {
    b = b.toLowerCase();
    return KEY_STATE[b] != null && KEY_STATE[b];
  }

  ns.text = function(msg, x, y, color) {
    msg   = msg   || '';
    x     = x     || 0;
    y     = y     || 0;
    color = color || 7;

    _ctx.fillStyle = COLORS[color];
    _ctx.fillText(msg, x, y + 5);
  }

  ns.cls = function(color) {
    color = color || 0;

    for (let x = 0; x < WIDTH; x++) {
      for (let y = 0; y < HEIGHT; y++) {
        ns.pset(x, y, color);
      }
    }
  }

  ns.pset = function(x, y, color) {
    if (x < 0 || x >= WIDTH || y < 0 || y >= HEIGHT) {
      return;
    }

    x = Math.round(x);
    y = Math.round(y);

    let i = (y * WIDTH  * 4) + (x * 4);

    _buffer[i    ] = COLORS[color][0];
    _buffer[i + 1] = COLORS[color][1];
    _buffer[i + 2] = COLORS[color][2];
    _buffer[i + 3] = 255;
  }

  ns.rect = function(x, y, width, height, color) {
    color  = color || 0;
    x      = Math.round(x);
    y      = Math.round(y);
    width  = Math.round(width);
    height = Math.round(height);

    for (let i = x; i < x + width; i++) {
      pset(i, y             , color);
      pset(i, y + height - 1, color);

    }
    
    for (let i = y; i < y + height; i++) {
      pset(x            , i, color);
      pset(x + width - 1, i, color);
    }
  }

  ns.rectfill = function(x, y, width, height, color) {
    color  = color || 0;
    x      = Math.round(x);
    y      = Math.round(y);
    width  = Math.round(width);
    height = Math.round(height);

    for (let px = x; px < x + width; px++) {
      for (let py = y; py < y + height; py++) {
        pset(px, py, color);
      }
    }
  }

  ns.circ = function(x, y, radius, color) {
    color  = color || 0;
    x      = Math.round(x);
    y      = Math.round(y);
    radius = Math.round(radius);

    for (let r = 0; r < Math.PI / 2; r += 1 / radius) {
      let qx = Math.round(Math.cos(r) * radius) + x;
      let qy = Math.round(Math.sin(r) * radius) + y;

      pset(qx          , qy          , color);
      pset(x - (qx - x), qy          , color);
      pset(x - (qx - x), y - (qy - y), color);
      pset(qx          , y - (qy - y), color);
    }

    pset(x, y + radius, color);
    pset(x, y - radius, color);
  }

  ns.circfill = function(x, y, radius, color) {
    color  = color || 0;
    x      = Math.round(x);
    y      = Math.round(y);
    radius = Math.round(radius);

    for (let r = 0; r < Math.PI / 2; r += 1 / radius) {
      let qx = Math.round(Math.cos(r) * radius) + x;
      let qy = Math.round(Math.sin(r) * radius) + y;

      for (let iy = y - (qy - y); iy <= qy; iy++) {
        pset(qx          , iy, color);
        pset(x - (qx - x), iy, color);
      }
    }

    for (let iy = y - radius; iy <= y + radius; iy++) {
      pset(x, iy, color);
    }
  }

  ns.line = function(x1, y1, x2, y2, color) {
    color = color || 0;
    x1    = Math.round(x1);
    y1    = Math.round(y1);
    x2    = Math.round(x2);
    y2    = Math.round(y2);

    let dx    = Math.abs(x2 - x1);
    let dy    = Math.abs(y2 - y1);
    let slope = (y2 - y1) / (x2 - x1);
    let sx    = x2 > x1 ? 1 : -1;
    let sy    = y2 > y1 ? 1 : -1;
    let px    = x1;
    let py    = y1;
    let b     = y1 - slope * x1;

    if (dx > dy) {
      while (px !== x2) {
        let actualY = b + slope * px;
        if (Math.abs(py - actualY) > 0.5) {
          py += sy;
        }
        px += sx;

        pset(px, py, color);
      }
    } else {
      while (py !== y2) {
        let actualX = (py - b) / slope;
        if (Math.abs(px - actualX) > 0.5) {
          px += sx;
        }
        py += sy;

        pset(px, py, color);
      }
    }

    pset(x1, y1, color);
    pset(x2, y2, color);
  }
})(window);

