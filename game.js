var SPEED = 1;
var _x = 75;
var _y = 75;

function update() {
  if (btn('w')) {
    _y -= SPEED;
  }
  if (btn('a')) {
    _x -= SPEED;
  }
  if (btn('s')) {
    _y += SPEED;
  }
  if (btn('d')) {
    _x += SPEED;
  }
}

function draw() {
  cls();
  rect(1, 1, 15, 10, 7);
  rectfill(1, 12, 15, 10, 7);
  circ(8, 30, 7, 7);
  circfill(8, 46, 7, 7);
  line(1, 55, 15, 69, 7);
  line(1, 69, 15, 55, 7);

  line(64, 64, _x, _y, 7);
  circfill(64, 64, 3, 5);
  circfill(_x, _y, 2, 8);
}
